package net.thumbtack.trainee.tanks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Runner {
	private static final Logger log = LoggerFactory.getLogger(Runner.class);
	
	public static void main(String[] args) {
		log.info("Start server");
		
		log.debug("Stop server");
	}
}
